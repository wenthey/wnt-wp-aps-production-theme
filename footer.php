<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

        <!-- APS pre-footer -->
        <div id="footer" class="aps-footer">
            <div class="row expanded">
                <div class="small-12 medium-6 columns aps-footer-section left">
                    <div class="aps-footer-rights">
                        <p><?php pll_e("copyright"); ?></p>
                    </div>
                </div>
                <div class="small-12 medium-6 columns aps-footer-section right">
                    <div>
                        <div class="aps-footer-social">
                            <a href="<?php echo pll_e("facebook_url"); ?>" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href="<?php echo pll_e("twitter_url"); ?>" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href="<?php echo pll_e("instagram_url"); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                        <div class="aps-footer-credits">
                            <!--<p>Created by</p>-->
                            <a href="http://www.wenthey.com"><img src="/wp-content/uploads/global/wnt-logo.svg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

		</div>
	</div>
</div>



<script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
<script>
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyB-iPVktDbGr0sGXvm0x_P0zUiaLNVAwoo",
        authDomain: "aps-website-169717.firebaseapp.com",
        databaseURL: "https://aps-website-169717.firebaseio.com",
        projectId: "aps-website-169717",
        storageBucket: "aps-website-169717.appspot.com",
        messagingSenderId: "116281491015"
    };
    firebase.initializeApp(config);
</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-102114932-1', 'auto');
    ga('send', 'pageview');

</script>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
