<?php
/**
 * Register Menus
 *
 * @link http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

register_nav_menus(array(
	'aps_nav_products'  => 'APS product navigation',
	'aps_nav_options' => 'APS options navigation',
));


/**
 * APS product navigation
 *
 * @link http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
if ( ! function_exists( 'aps_nav_products' ) ) {
	function aps_nav_products() {
		wp_nav_menu( array(
			'container'      => false,
			'menu_class'     => '',
			'items_wrap'     => '<ul>%3$s</ul>',
			'theme_location' => 'aps_nav_products',
			'depth'          => 0,
			'fallback_cb'    => false,
			'walker'         => new Foundationpress_Top_Bar_Walker(),
		));
	}
}

/** Mobile variant **/
if ( ! function_exists( 'aps_nav_products_mobile' ) ) {
    function aps_nav_products_mobile() {
        wp_nav_menu( array(
            'container'      => false,
            'menu_class'     => 'vertical menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'aps_nav_products',
            'depth'          => 0,
            'fallback_cb'    => false,
            'walker'         => new Foundationpress_Top_Bar_Walker(),
        ));
    }
}

/**
 * APS options navigation
 */
if ( ! function_exists( 'aps_nav_options' ) ) {
    function aps_nav_options() {
        wp_nav_menu( array(
            'container'      => false,
            'menu_class'     => '',
            'items_wrap'     => '<ul>%3$s</ul>',
            'theme_location' => 'aps_nav_options',
            'depth'          => 0,
            'fallback_cb'    => false,
            'walker'         => new Foundationpress_Top_Bar_Walker(),
        ));
    }
}


/** Mobile variant **/
if ( ! function_exists( 'aps_nav_options_mobile' ) ) {
    function aps_nav_options_mobile() {
        wp_nav_menu( array(
            'container'      => false,
            'menu_class'     => 'vertical menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'aps_nav_options',
            'depth'          => 0,
            'fallback_cb'    => false,
            'walker'         => new Foundationpress_Top_Bar_Walker(),
        ));
    }
}