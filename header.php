<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>
	<?php do_action( 'foundationpress_layout_start' ); ?>
    <!-- TODO - figure out what are these actions -->

    <div class="off-canvas-wrapper">
        <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
            <div class="off-canvas-content" data-off-canvas-content>
<!-- Header start-->
                <header id="header">
                    <div class="expanded row">
                        <div class="show-for-medium">

                            <div class="small-12 medium-12 large-12 columns aps-top-nav-wrap">
                                <nav id="aps-nav-options-desktop">
<!-- Desktop nav - Language -->
                                    <div class="aps-lang-switch">
                                        <i class="fa fa-globe"></i>
                                        <?php pll_the_languages();?>
                                    </div>
<!-- Desktop nav - Options -->
                                    <?php aps_nav_options(); ?>
                                </nav>
<!-- Desktop nav - Products -->
                                <div id="aps-nav-products-desktop">
                                <div class="float-left aps-logo">
                                    <a href="<?php echo esc_url( pll_home_url() ); ?>">
                                        <img src="/wp-content/uploads/global/aps-logo-iron.svg"
                                             alt="APS - Devoted to unforgettable experience">
                                    </a>
                                </div>
                                    <?php aps_nav_products(); ?>
                                </div>
                            </div>
                        </div>

<!-- Responsive nav - off-canvas -->
                        <div class="hide-for-large">

<!-- Top bar with off-canvas trigger button -->
                            <div class="title-bar">
                                <div class="float-left aps-logo">
                                    <a href="<?php echo esc_url( pll_home_url() ); ?>">
                                        <img src="/wp-content/uploads/global/aps-logo-iron.svg" alt="APS - Devoted to
                                        unforgettable experience"> <!-- TODO - translate ALTs -->
                                    </a>
                                </div>
                                <button id="open-menu" type="button" class="float-right" data-toggle="offCanvas">
                                    <i class="fa fa-bars" aria-hidden="true"></i>
                                </button>
<!-- Scroll to top button -->
                                <button id="scroll-to-top" type="button" class="float-right scroll-to-top">
                                <span class="button skinny screamin-green"><i class="fa fa-level-up" aria-hidden="true"></i>
                                    <?php pll_e("back_to_top"); ?>
                                </span>
                                </button>
                            </div>

<!-- Off-canvas navigation -->
                            <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
                                <nav id="aps-nav-mobile">
                                    <button class="close-button" aria-label="Close menu" type="button">
                                        <i class="fa fa-globe" aria-hidden="true"><span><?php pll_e("choose_language"); ?></span></i>
                                    </button>

                                    <ul class="vertical menu">
                                        <?php pll_the_languages();?>
                                    </ul>

<!-- Off-canvas close button -->
                                    <button class="close-button" aria-label="Close menu" type="button" data-close>
                                        <i class="fa fa-arrow-circle-left" aria-hidden="true"><span><?php pll_e("hide_menu"); ?></span></i>
                                    </button>

<!-- Off-canvas items -->
                                    <?php aps_nav_options_mobile(); ?>

                                    <button class="close-button" aria-label="Close menu" type="button">
                                        <i class="fa fa-asterisk" aria-hidden="true"><span><?php pll_e("our_products");
                                        ?></span></i>
                                    </button>

                                    <?php aps_nav_products_mobile(); ?>

                                </nav>
                            </div>
                        </div>

                    </div>
                </header>
    
