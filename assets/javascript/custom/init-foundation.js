jQuery(document).foundation();


$(document).ready(function() {
    
    // Scroll to top
    var offset = 210;
    var duration = 300;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.scroll-to-top').fadeIn(duration);
        } else {
            $('.scroll-to-top').fadeOut(duration);
        }
    });
    
    $('.scroll-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    });
    
    // Magnifico popup
    $('.aps-product-description-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        // mainClass: 'mfp-img-mobile',
        mainClass: 'mfp-fade',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return '<h4 class="lb-title">' + item.el.attr('title') + '</h4>' + '<p class="lb-description">' + item.el.attr('data-description') + '</p>';
            }
        }
    });
    
    // Headroom init
    // grab an element
    var elem = document.querySelector("header");
    // construct an instance of Headroom, passing the element
    var headroom = new Headroom(elem, {
        offset: 210,
        tolerance: 5,
        classes: {
            initial: "animated",
            pinned: "slideDown",
            unpinned: "slideUp"
        }
    });

    // Check whether visitor's device is a mobile device or desktop
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    $(document).ready(function($){
        if(!isMobile) {
            // initialise
            headroom.init();
        } else {
            console.log('No headroom for mobile device');
        }
    });
});

