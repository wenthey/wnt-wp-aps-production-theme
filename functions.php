<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );

/** Add support for SVG files upload */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/** Update Google Maps JavaScript API */
function my_acf_init() {

    acf_update_setting('google_api_key', 'AIzaSyB-iPVktDbGr0sGXvm0x_P0zUiaLNVAwoo');
}

add_action('acf/init', 'my_acf_init');

/** Enable numbered pagination */
function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    $translation_pages_word = pll__("pager_title");
    $translation_pages_from = pll__("pager_operator");

    if(1 != $pages)
    {
        echo "<div class=\"aps-pagination\"><span class=\"aps-pagination-page-count\">" . $translation_pages_word . " " . $paged . " " . $translation_pages_from . " " .$pages . "</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'><i class=\"fa fa-angle-double-left\" aria-hidden=\"true\"></i></a>";
        if($paged > 1) echo "<a href='".get_pagenum_link($paged - 1)."'><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></a>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"aps-pagination-current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }

        if ($paged < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\"><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'><i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i></a>";
        echo "</div>\n";
    }
};

///** Magnifico linking dependencies */
//function magnific_popup_files() {
//    wp_enqueue_style( 'magnific', get_template_directory_uri() . '/css/magnific-popup-gallery.css' );
//    wp_enqueue_script( 'magnific', get_template_directory_uri() .'/js/magnific-popup-gallery.min.js', array('jquery'), false, true );
//}
//
//add_action( 'wp_enqueue_scripts', 'magnific_popup_files' );

/** Register strings for translation */

/** Top menu bar */
pll_register_string("Mobile - Menu", "back_to_top", "aps-theme-string", false);
pll_register_string("Mobile - Menu", "hide_menu", "aps-theme-string", false);
pll_register_string("Mobile - Menu", "choose_language", "aps-theme-string", false);
pll_register_string("Mobile - Menu", "our_products", "aps-theme-string", false);

/** Footer */
pll_register_string("General - Footer", "copyright", "aps-theme-string", false);

/** Home page */
pll_register_string("General - Articles header", "whats_up", "aps-theme-string", false);

/** Articles */
pll_register_string("General - Article", "read_more", "aps-theme-string", false);

/** Product page */
pll_register_string("General - Product page specs link", "download_specs", "aps-theme-string", false);
pll_register_string("General - Product page manual link", "download_manual", "aps-theme-string", false);
pll_register_string("General - Product page design title", "product_design", "aps-theme-string", false);
pll_register_string("General - Product page finishes title", "product_finishes", "aps-theme-string", false);
pll_register_string("General - Product page gallery title", "product_gallery", "aps-theme-string", false);
pll_register_string("General - Product page testimonials title", "product_testimonials_header", "aps-theme-string", false);
pll_register_string("General - We are APS text", "prefooter_txt", "aps-theme-string", true);
pll_register_string("General - We are APS URL", "prefooter_url", "aps-theme-string", false);
pll_register_string("General - We are APS button", "prefooter_btn", "aps-theme-string", false);

/** Other pages */

pll_register_string("General - News page title", "news_title", "aps-theme-string", false);
pll_register_string("General - News page pager", "pager_title", "aps-theme-string", false);
pll_register_string("General - News page pager", "pager_operator", "aps-theme-string", false);

/** Social media URLs **/
pll_register_string("General - We are APS URL", "facebook_url", "aps-theme-string", false);
pll_register_string("General - We are APS URL", "twitter_url", "aps-theme-string", false);
pll_register_string("General - We are APS URL", "instagram_url", "aps-theme-string", false);
