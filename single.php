<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="aps-post-section">
    <article class="column aps-about-content" id="post-<?php the_ID(); ?>">

    <?php while ( have_posts() ) : the_post(); ?>

        <div class="row">
            <div class="aps-page-top-offset"></div>
        </div>
        <div class="row">
            <div class="aps-global-heading">
                <h3 class="screamin-green"><?php the_title(); ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="aps-global-paragraph aps-paragraph-box">
                <?php the_content(); ?>
            </div>
        </div>

    </article>

    <?php endwhile;?>

</div>

<!-- Pre-footer -->
<div class="aps-pre-footer" style="background-image: url('/wp-content/uploads/global/aps-pre-footer-bckg@2x.jpg');">
    <div class="row">
        <div class="small-12 medium-10 medium-centered columns centered-text">
            <p><?php pll_e("prefooter_txt"); ?></p>
            <a href="<?php echo pll_e("prefooter_url"); ?>" class="button fat screamin-green-full"><?php pll_e("prefooter_btn"); ?></a>
        </div>
    </div>
</div>
<?php get_footer();
