<?php
/*
Template Name: APS - Contact page
*/
get_header(); ?>

<?php get_template_part( 'template-parts/page-featured-image' ); ?>
    <div class="column aps-about-content">
        <div class="row">
            <div class="aps-page-top-offset"></div>
        </div>
        <div class="row">
            <div class="aps-global-heading">
                <h3 class="screamin-green"><?php the_title(); ?></h3>
            </div>
        </div>

        <div class="row aps-paragraph-box">

            <div class="small-12 medium-4 columns">
                <div class="aps-global-paragraph aps-contact-details-txt">
                    <?php the_field('contact_company_details'); ?>
                </div>
            </div>
            <div class="small-12 medium-8 columns">

                <div><?php the_field('contact_form_code'); ?></div>

            </div>

        </div>

        </div>
    </div>
</div>

<?php get_footer();
