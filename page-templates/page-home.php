<?php
/*
Template Name: APS - Home page
*/
get_header(); ?>

<?php get_template_part( 'template-parts/page-featured-image' ); ?>
        <div class="column aps-home-content">
            <div class="row">
                <div class="small-12 columns aps-global-shout">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns aps-global-paragraph text-center">
                    <?php

                    $pageid = get_the_id();
                    $content_post = get_post($pageid);
                    $content = $content_post->post_content;
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);

                    echo $content;

                    ?>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns aps-global-btns">
                    <a href="<?php the_field('page_button_link_url'); ?>" class="button fat screamin-green-full"><?php the_field('page_button_link_text'); ?></a>
                </div>
            </div>
            <div class="row aps-articles" data-equalizer>
                <div class="aps-global-heading">
                    <h2><?php pll_e("whats_up"); ?></h2>
                </div>

                <?php
                // Open post query
                $args = array( 'posts_per_page' => 2, 'post_type' => 'post', 'orderby' => 'date', 'order' => 'DESC', );
                $posts = new WP_Query( $args );

                if ( $posts->have_posts() ) : while ( $posts->have_posts() ) : $posts->the_post(); ?>
                
                <div class="small-12 medium-6 columns aps-article-tile" data-equalizer-watch>
                    <div class="aps-article-tile-content">
                        <div class="aps-article-tile-title">
                            <h2><?php the_title(); ?></h2>
                        </div>
                        <div class="aps-article-tile-paragraph">
                            <p><?php echo get_the_excerpt() ;?></p>
                            <a href="<?php the_permalink();?>" class="button slim screamin-green-full"><?php pll_e("read_more"); ?></a>
                        </div>
                    </div>
                </div>

                <?php endwhile; endif; ?>

            </div>
        </div>
    </div>
</div>

<?php get_footer();
