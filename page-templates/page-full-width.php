<?php
/*
Template Name: Full Width
*/
get_header(); ?>

    <div class="aps-page-wrapper">
        <?php the_content(); ?>
    </div>

<?php get_footer();
