<?php
/*
Template Name: APS - Generic page
*/
get_header(); ?>

<?php get_template_part( 'template-parts/page-featured-image' ); ?>
    <div class="column aps-about-content">
        <div class="row">
            <div class="aps-page-top-offset"></div>
        </div>
        <div class="row">
            <div class="aps-global-heading">
                <h3 class="screamin-green"><?php the_field('generic_page_title'); ?></h3>
            </div>
        </div>
            <?php

            $gen_content = get_field('generic_page_element');

            if($gen_content)

            {

                foreach($gen_content as $gen_content_section)

                {

                    echo '<div class="row">'
                        . '<div class="aps-global-heading">'
                        . '<h3 class="screamin-green">' . $gen_content_section['generic_page_section_title']
                        . '</h3></div></div>'
                        . '<div class="row">'
                        . '<div class="aps-global-paragraph aps-paragraph-box">'
                        . $gen_content_section['generic_page_content'] . '</div></div>';

                }

            }

            ?>
    </div>
</div>

<?php get_footer();
