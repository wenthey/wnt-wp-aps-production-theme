<?php
/*
Template Name: APS - News page
*/
get_header(); ?>

<?php get_template_part( 'template-parts/page-featured-image' ); ?>
    <div id="aps-news-hero-image">
        <div class="aps-page-content-expand">
            <div class="row">
                <div class="aps-page-top-offset"></div>
            </div>
            <div class="row aps-articles" data-equalizer data-equalize-by-row="true">
                <div class="aps-global-heading">
                    <h3 class="screamin-green"><?php echo pll_e("news_title"); ?></h3>
                </div>

                    <?php

                    // Open post query
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array( 'posts_per_page' => 10, 'paged' => $paged,'post_type' => 'post' );
                    $postslist = new WP_Query( $args );

                    if ( $postslist->have_posts() ) :
                        while ( $postslist->have_posts() ) : $postslist->the_post();

                    ?>

                            <div class="small-12 medium-6 columns aps-article-tile" data-equalizer-watch>
                                <div class="aps-article-tile-content">
                                    <div class="aps-article-tile-title">
                                        <h2><?php the_title(); ?></h2>
                                    </div>
                                    <div class="aps-article-tile-paragraph">
                                        <p><?php echo get_the_excerpt() ;?></p>
                                        <a href="<?php the_permalink();?>" class="button slim screamin-green-full"><?php pll_e("read_more"); ?></a>
                                    </div>
                                </div>
                            </div>

                        <?php endwhile; ?>
                    <?php endif; ?>

                <div class="column aps-pagination-wrap">

                    <?php

                    // Numbered pagination
                    if (function_exists("pagination")) {
                        pagination($postslist->max_num_pages);
                    }

                    ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();
