<?php
/*
Template Name: APS - Product page
*/

get_header(); ?>

<?php get_template_part( 'template-parts/product-featured-image' ); ?>

        <div class="column">
<!-- Main product section -->
            <div class="row">
                <div class="small-12 columns aps-product-top">
<!-- Product logo -->
                    <?php

                    $image = get_field('product_logo');

                    if( !empty($image) ): ?>
                        <img src="<?php echo $image['url']; ?>" class="aps-product-logo" alt="<?php echo $image['alt']; ?>" />
                    <?php endif; ?>

                </div>
            </div>
            <div class="row">
                <div class="small-12 columns aps-product-intro">

                    <?php

                    $pageid = get_the_id();
                    $content_post = get_post($pageid);
                    $content = $content_post->post_content;
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);

                    echo $content;

                    ?>

                </div>
            </div>

            <div class="row">
                <div class="small-12 columns aps-product-docs">

<!-- Display spec sheet link if present -->
                    <?php

                    $specs_present = get_field( "product_specs_file" );
                    $specs_url = get_field('product_specs_file');

                    if($specs_present)

                    {

                        echo '<a href="' . $specs_url . '" target="_blank" class="button fat screamin-green-full">'
                            . pll__("download_specs") . '</a>';

                    } else {

                        echo '';

                    };

                    ?>

                    <?php

                    $manual_present = get_field( "product_manual_file" );
                    $manual_url = get_field('product_manual_file');

                    if($manual_present)

                    {

                        echo '<a href="' . $manual_url . '" target="_blank" class="button fat screamin-green-full">'
                            . pll__("download_manual") . '</a>';

                    } else {

                        echo '';

                    };

                    ?>

                    <a href="<?php the_field('product_button_link_url'); ?>" class="button fat screamin-green-full">
                        <?php the_field('product_button_link_text'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
<!-- 4 product USPs -->
    <div class="column row expanded aps-usps-wrap" data-equalizer>

        <?php

        $usps = get_field('product_feature');

        if($usps)

        {

            foreach($usps as $usp)

            {

                echo '<div class="small-12 medium-6 large-3 columns aps-usp-item" data-equalizer-watch><h3>'
                    . $usp['product_feature_text'] . '</h3>' . '</div>';

            }

        }

        ?>

    </div>

<!-- Detailed product description -->
    <div class="column row expanded aps-product-description-wrap clearfix" data-equalizer>
        <div class="row">
            <div class="small-12 large-6 columns aps-product-description-section" data-equalizer-watch>
                <h4><?php pll_e("product_design"); ?></h4>
                <?php the_field('product_design_description_text'); ?>
                <h4><?php pll_e("product_finishes"); ?></h4>
                <?php the_field('product_finishes_description_text'); ?>
            </div>
            <div class="small-12 large-6 columns aps-product-description-section" data-equalizer-watch>
                <h4><?php pll_e("product_gallery"); ?></h4>
                <div class="small-12 small-collapse columns aps-product-description-gallery clearfix">

<!-- Show the 1st product image -->
                    <?php

                    $images = get_field('product_images_gallery');
                    $image  = $images[0];

                    if( $image ) :

                    ?>
                        <div class="small-12 medium-6 medium-collapse columns">
                            <a href="<?php echo $image['url']; ?>"
                               class="lightbox-link"
                               title="<?php echo $image['caption']; ?>"
                               data-description="<?php echo $image['description']; ?>">

                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </a>
                        </div>

                    <?php endif; ?>

<!-- Show the 2nd product image -->
                    <?php

                    $images = get_field('product_images_gallery');
                    $image  = $images[1];

                    if( $image ) :

                    ?>
                        <div class="small-12 medium-6 medium-collapse columns">
                            <a href="<?php echo $image['url']; ?>"
                               class="lightbox-link"
                               title="<?php echo $image['caption']; ?>"
                               data-description="<?php echo $image['description']; ?>">

                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </a>
                        </div>

                    <?php endif; ?>

<!-- Show the 3rd product image -->
                    <?php

                    $images = get_field('product_images_gallery');
                    $image  = $images[2];

                    if( $image ) :

                    ?>
                        <div class="small-12 medium-12 medium-collapse columns">
                            <a href="<?php echo $image['url']; ?>"
                               class="lightbox-link"
                               title="<?php echo $image['caption']; ?>"
                               data-description="<?php echo $image['description']; ?>">

                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </a>
                        </div>

                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>

<!-- Product testimonials -->
        <?php

        $testimonial_present = get_field( "single_product_testimonial" );

        if( $testimonial_present ) {

            echo '<div class="column row expanded aps-testimonial-wrap clearfix">'
                .'<div class="row">'
                . '<div class="large-12 columns centered-text">'
                . '<h2>' . pll__("product_testimonials_header") . '</h2>'
                . '</div>' . '</div>'
                . '<div class="row" data-equalizer data-equalize-by-row="true">';

            $testimonials = get_field('single_product_testimonial');

            if($testimonials)

            {

                foreach($testimonials as $testimonial)

                {

                    echo '<div class="medium-6 columns testimonial" data-equalizer-watch>'
                        . '<div class="aps-testimonial-quote">' . $testimonial['product_testimonial_content'] . '</div>'
                        . '<div class="aps-testimonial-author">'
                        . '<div class="aps-testimonial-photo">'
                        . '<img src="' . $testimonial['product_testimonial_author_image'] . '" />'
                        . '</div>'
                        . '<p>' . $testimonial['product_testimonial_author'] . '</p>'
                        . '<p>' . $testimonial['product_testimonial_author_company'] . '</p>'
                        . '</div>' . '</div>';

                }

            };

        echo '</div>' . '</div>';

        } else {

            echo '';

        };

        ;?>

<!-- Pre-footer -->
    <div class="aps-pre-footer" style="background-image: url('/wp-content/uploads/global/aps-pre-footer-bckg@2x.jpg');">
        <div class="row">
            <div class="small-12 medium-10 medium-centered columns centered-text">
                <p><?php pll_e("prefooter_txt"); ?></p>
                <a href="<?php echo pll_e("prefooter_url"); ?>" class="button fat screamin-green-full"><?php pll_e("prefooter_btn"); ?></a>
            </div>
        </div>
    </div>

<?php get_footer();
